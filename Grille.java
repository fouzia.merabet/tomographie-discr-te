import java.io.BufferedReader;
import java.awt.*;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class Grille {

	static int[][] grille;
	static int[][] instances;
	static String[][] TT;
	static int m = 0;
	static int n = 0;
        static int nb=0 ;
    
	/**************************************************************/
	/*** la fonction qui affiche l'interface de la grille ***/
	/**************************************************************/

	public static void AfficherEnInterfaceGrille() {
		Vue vue = new Vue(n, m);
		for (int i = 0; i < n; i++) {

			for (int j = 0; j < m; j++) {
				if (grille[i][j] == 1)
					vue.boutons[i][j].setBackground(Color.white);
				if (grille[i][j] == 2)
					vue.boutons[i][j].setBackground(Color.black);

			}

		}

	}

	/************************************************************************/
	/** la fonction qui lit le fichier et remplie le tableau des instances ***/
	/**********************************************************************/

	public static void lireInstance(String fichier) throws IOException {

		// lecture du fichier d'instances
		InputStream in = new FileInputStream(fichier);
		BufferedReader file = new BufferedReader(new InputStreamReader(in));

		// recuperer la valeur de n et m
		String ligne = file.readLine();
		String[] t = ligne.split(" ");
		n = Integer.parseInt(t[0]);
		m = Integer.parseInt(t[1]);
		instances = new int[n + m][];
		System.out.println("\nLa grille est de: ");
		System.out.println("Hauteur n= " + n);
		System.out.println("Largeur m= " + m);
		System.out.println("\nLes séquences: ");

		int i = 0;
		while ((ligne = file.readLine()) != null && i < (n + m)) {
			if (!ligne.equals("")) {
				String[] tab = ligne.split(" ");
				int taille = 0;		        
				for (int j = 0; j < tab.length; j++) {			       
				    if (!tab[j].equals(" ") && !tab[j].equals("")){			        	
						taille++;
				    }	        

				}

				int[] tabint = new int[taille]; // tableau contenant les
												// valeurs
												// entieres des sequences
				int k = 0; // indice pour parcourir tabint
		        
				for (int j = 0; j < tab.length; j++) {
					if (!tab[j].equals(" ") && !tab[j].equals("")) {					  
						tabint[k] = Integer.parseInt(tab[j]);
						System.out.print("  " + tabint[k]);
					       	k++;
					   			        
					}

				}
				instances[i] = tabint;
				if (i == n - 1)
					System.out.println();
				System.out.println();
				i++;
			}
		}
//initialisation de la grille
		grille = new int[n][m];
		for (int i1 = 0; i1 < n; i1++) {
			for (int j = 0; j < m; j++) {
				grille[i1][j] = 0;
			}
		}


	}

	/***************************************************************/

	static void Affichegrille() {
		for (int i = 0; i < n; i++) {

			for (int j = 0; j < m; j++) {
			    if(grille[i][j]==2)
				System.out.print("*");
			      else 	System.out.print(" ");
		        
			}
			System.out.println();

		}
	}

	/***************************************************************/

	static boolean Compare_seq_ligne(int index) {

		int seq = 0;
		int j = 0;
		int cpt = 0;

		for (int k = 1; k < instances[index].length; k++) {
			seq = instances[index][k];

			while (j < m && grille[index][j] == 1) {

				j++;
			}
			cpt = 0;
			if (j < m) {
				if (grille[index][j] == 0)
					return false;

				while (j < m && grille[index][j] == 2) {

					j++;
					cpt++;
				}
				if (cpt != seq)
					return false;

				else {
					if ((j < m && grille[index][j] != 1) || (j == m && k != instances[index].length - 1))
						return false;

				}
			} else {

				if ((k != (instances[index].length - 1)) || cpt != seq)
					return false;
			}

		}
		return true;
	}

	/***************************************************************/

	static boolean Compare_seq_col(int j) {
		int index = j + n;
		int seq = 0;
		int i = 0;

		int cpt = 0;
		for (int k = 1; k < instances[index].length; k++) {
			seq = instances[index][k];

			if (i == n)
				return false;
			while (i < n && grille[i][j] == 1) {

				i++;
			}
			cpt = 0;
			if (i < n) {
				if (grille[i][j] == 0)
					return false;

				while (i < n && grille[i][j] == 2) {
					i++;
					cpt++;
				}
				if (cpt != seq)
					return false;
				else {
					if ((i < n && grille[i][j] != 1) || (i == n && k != instances[index].length - 1))
						return false;

				}
			} else {
				if ((k != (instances[index].length - 1)) || cpt != seq)
					return false;
			}

		}

		return true;

	}

	/***************************************************************/
	static boolean Enumeration_Rec(int k, int c) {
		boolean ok;
		boolean raz;
		int i, j;
		i = k / m;
		j = k % m;
		if (grille[i][j] == 0) {
			grille[i][j] = c;
			raz = true;
		} else {
			if (grille[i][j] != c) {
				return false;
			} else
				raz = false;
		}
		ok = true;
		if (i == n - 1)
			ok = Compare_seq_col(j);
		if (ok && j == m - 1)
			ok = Compare_seq_ligne(i);
		if (ok) {

			if (i == n - 1 && j == m - 1)
				return true;
			ok = Enumeration_Rec(k + 1, 1) || Enumeration_Rec(k + 1, 2);
		}
		if ((!ok) && raz)
			grille[i][j] = 0;
		return ok;
	}
    
	/***************************************************************/
	/*** question 11 **/
	/***************************************************************/
	static boolean TestSiAucun(int ligne, int j1, int j2, int c) {
		for (int i = j1; i <= j2; i++) {
			if (grille[ligne][i] == c)
				return false;
		}
		return true;
	}

	/***************************************************************/

	static boolean TestVecteurLigne_Rec(int ligne, int j, int l) {

		boolean c1;
		boolean c2;
		boolean a=initialisationLigne(ligne);
		if (l == 0)
			return TestSiAucun(ligne, 0, j, 2);
		if ((l == 1) && (j == (instances[ligne][l] - 1)))
			return TestSiAucun(ligne, 0, j, 1);
		if (j <= (instances[ligne][l] - 1))
			return false;

		if (!TT[j][l].equals("nonvisite")) {
			if (TT[j][l].equals("vrai")) {
				return true;
			} else {
				return false;
			}
		}
		if (grille[ligne][j] == 2)
			c1 = false;
		else
			c1 = TestVecteurLigne_Rec(ligne, j - 1, l);
		if (!TestSiAucun(ligne, j - (instances[ligne][l] - 1), j, 1))
			c2 = false;
		else {
			if (grille[ligne][j - instances[ligne][l]] == 2)
				c2 = false;
			else
				c2 = TestVecteurLigne_Rec(ligne, j - instances[ligne][l] - 1, l - 1);
		}
		if (c1 || c2) {
			TT[j][l] = "vrai";
			return true;
		} else {
			TT[j][l] = "faux";
			return false;
		}
	}


	

	/***************************************************************/

    
		static boolean TestSiAucunCol(int col, int j1, int j2, int c) {
		for (int i = j1; i <= j2; i++) {
			if (grille[i][col] == c)
				return false;
		}
		return true;
	}

	/***************************************************************/

	static boolean TestVecteurColonne_Rec(int col, int j, int l) {

		boolean c1;
		boolean c2;
		boolean a=initialisationColonne(col);
		if (l == 0)
			return TestSiAucunCol(col, 0, j, 2);
		if ((l == 1) && (j == (instances[n + col][l] - 1)))
			return TestSiAucunCol(col, 0, j, 1);
		if (j <= (instances[n + col][l] - 1))
			return false;
		if (!TT[j][l].equals("nonvisite")) {
			if (TT[j][l].equals("vrai")) {
				return true;
			} else {
				return false;
			}
		}
		if (grille[j][col] == 2)
			c1 = false;
		else
			c1 = TestVecteurColonne_Rec(col, j - 1, l);
		if (!TestSiAucunCol(col, j - (instances[n + col][l] - 1), j, 1))
			c2 = false;
		else {
			if (grille[j - instances[n + col][l]][col] == 2)
				c2 = false;
			else
				c2 = TestVecteurColonne_Rec(col, j - instances[n + col][l] - 1, l - 1);
		}
		if (c1 || c2) {
			TT[j][l] = "vrai";
			return true;
		} else {
			TT[j][l] = "faux";
			return false;
		}
	}


    /********************************************************************/
    static boolean  initialisationLigne(int i){
	int taille=instances[i].length;
	TT=new String [m][];
		for (int i1 = 0; i1 < m; i1++) {
		    TT[i1]=new String [taille];
			for (int j = 0; j < taille; j++) {
				TT[i1][j] = "nonvisite";
			}
		}
		return true;	
    }
    /***********************************/
  static boolean initialisationColonne(int col){
        int taille=instances[col+n].length;
	TT=new String [n][];
		for (int i1 = 0; i1 < n; i1++) {
		    TT[i1]=new String [taille];
			for (int j = 0; j < taille; j++) {
				TT[i1][j] = "nonvisite";
			}
		}
		return true;    
    }


	/********************************************************************/

    static boolean PropagLigne(int i, boolean[] marque,int nb) {
		int j;
		boolean c1, c2;
		int cptcolor;
		cptcolor = 0;
		nb = 0;
		for (j = 0; j < m; j++) {
			if (grille[i][j] == 0) {
				grille[i][j] = 1;
				c1 = TestVecteurLigne_Rec(i, m - 1, instances[i].length - 1);
				grille[i][j] = 2;				
				c2 = TestVecteurLigne_Rec(i, m - 1, instances[i].length - 1);
				grille[i][j] = 0;
				if ((!c1) && (!c2))
					return false;
				if (c1 && (!c2)) {
					grille[i][j] = 1;
					cptcolor = cptcolor + 1;
					if (!marque[j]) {
						marque[j] = true;
						nb = nb + 1;
					}
				}
				if ((!c1) && c2) {
					grille[i][j] = 2;
					cptcolor = cptcolor + 1;
					if (!marque[j]) {
						marque[j] = true;
						nb = nb + 1;
					}
				}
			}
		}
		return true;
	}

	/***************************************************************/
    
    /**************************************************************/
    static boolean PropagCol(int j, boolean[] marque,int nb) {
		int i;
		boolean c1, c2;
		int cptcolor;
		cptcolor = 0;
		nb = 0;
		for (i = 0; i < n; i++) {
			if (grille[i][j] == 0) {
				grille[i][j] = 1;
				c1 = TestVecteurColonne_Rec(j, n - 1, instances[j + n].length - 1);
				grille[i][j] = 2;
				c2 = TestVecteurColonne_Rec(j, n - 1, instances[j + n].length - 1);
				grille[i][j] = 0;
				if ((!c1) && (!c2)){
					return false;
				}
				if (c1 && (!c2)) {
					grille[i][j] = 1;
					cptcolor = cptcolor + 1;
					if (!marque[i]) {
						marque[i] = true;
						nb = nb + 1;
					}
				}
				if ((!c1) && c2) {
					grille[i][j] = 2;
					cptcolor = cptcolor + 1;
					if (!marque[i]) {
						marque[i] = true;
						nb = nb + 1;
					}
				}
			}
		}
		return true;
	}

	/***************************************************************/
	static boolean Propagation() {
		boolean[] marqueL;
		boolean[] marqueC;
		int nbmL, nbmC;
		int i, j ;
		boolean ok;
        
		ok = true;
		marqueL = new boolean[n];
		for (int k = 0; k < n; k++) {
			marqueL[k] = true;
		}
		marqueC = new boolean[m];
		for (int k = 0; k < m; k++) {
			marqueC[k] = true;
		}
		nbmL = n;
		nbmC = m;
		while (ok && ((nbmL != 0) || (nbmC != 0))) {
			i = 0;
			while (ok && i < n) {
				if (marqueL[i]) {
				    ok = PropagLigne(i, marqueC,nb);
					nbmC = nbmC + nb;
					marqueL[i] = false;
					nbmL = nbmL - 1;
				}
				i = i + 1;
			}
			j=0;
			while (ok && j < m) {
				if (marqueC[j]) {
				    ok = PropagCol(j, marqueL,nb);
					nbmL = nbmL + nb;
					marqueC[j] = false;
					nbmC = nbmC - 1;
				}
				j = j + 1;
			}
		}
		return ok;
	}

	/*******************************************************************/

}
