import java.io.IOException;
import java.lang.management.ManagementFactory;
import java.lang.management.ThreadMXBean;


public class TestEnumeration {

	public static void main(String[] args) throws IOException {
		if(args.length==1){
			Grille grille = new Grille();
			String chaine = args[0];
			try{
			    ThreadMXBean thread = ManagementFactory.getThreadMXBean();
			    grille.lireInstance(chaine);
			    boolean a=grille.Enumeration_Rec(0, 1) || grille.Enumeration_Rec(0, 2);
			        
			    grille.AfficherEnInterfaceGrille();
			    long first = thread.getCurrentThreadCpuTime();
			    System.out.println("la duree est de : "+first);
			}catch(Exception e){
			    System.out.println("Le fichier specifie est introuvable!!!!");
			}

		}else{
		    System.out.println("USAGE: java TestEnumeration inputFile");
	        

		}
	}
}
