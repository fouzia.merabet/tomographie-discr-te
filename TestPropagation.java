import java.io.IOException;


public class TestPropagation {
	public static void main(String[] args) throws IOException {
	    if(args.length==1){
		Grille grille = new Grille();
		String chaine = args[0];		
		double cpt=0;
	       
		    
		    long startTime = System.currentTimeMillis();
		
			grille.lireInstance(chaine);

			boolean a=grille.Propagation();
			//boolean ok=grille.Enumeration_Rec(0,1)|| grille.Enumeration_Rec(0,2);
		        long endTime = System.currentTimeMillis();
			System.out.println("La duree est de : "+(endTime-startTime));
			for (int i1 = 0; i1 <grille.n; i1++) {
				for (int j = 0; j < grille.m; j++) {
					if(grille.grille[i1][j] != 0)
						cpt++;
				}
			}
			grille.AfficherEnInterfaceGrille();
			System.out.println("pourcentage= "+cpt/(grille.n*grille.m)*100+"%");
				
	
	    }else{
		    System.out.println("USAGE: java TestPropagation inputFile");
	        

		}	
	}
}
