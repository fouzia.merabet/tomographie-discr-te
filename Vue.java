import java.awt.Container;
import java.awt.GridLayout;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;



 class Vue extends JFrame {
		private static final int dimBouton = 100; // largueur de la fenetre

		
		public JButton[][] boutons;
		
		Container panneau;
		

		// constructeur de la classe interne
		Vue(int n, int m) {
			super();
			// taille fenetre, titre..
			setSize(dimBouton * n, dimBouton *m);
			setTitle("Grille");
			setDefaultCloseOperation(EXIT_ON_CLOSE);

			// On récupère le panneau attaché à la fenêtre
			panneau = getContentPane();

			// indique que la mise en page est une grille de dim X dim
			panneau.setLayout(new GridLayout(n, m));

			// et on place les boutons dans chaque case.
			boutons = new JButton[n][m];
			for (int i = 0; i < n; i++) {
				for (int j = 0; j < m; j++) {

					boutons[i][j] = new JButton();
					// ajoute le bouton dans la case
					panneau.add(boutons[i][j]);

				}
			}

			setVisible(true);
		}
 }

		/***************************************************************/

